﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IceCreamStore
{
    public partial class Form1 : Form
    {
        MdiContainerForm _parent;
        public Form1(MdiContainerForm parent)
        {
            _parent = parent;
            InitializeComponent();
            InitializeSelections();

            listBoxFlavors.Focus();
        }

        private void InitializeSelections()
        {
            listBoxFlavors.SelectedIndex = 0;
            radioButton1Scoop.Checked = true;

            checkBoxSprinkles.Checked = false;
            checkBoxOreos.Checked = false;
            checkBoxChocolateChips.Checked = false;
            checkBoxCherrySyrup.Checked = false;
            checkBoxGummyWorms.Checked = false;
            DisplayTotal();
        }

        private void DisplayTotal()
        {
            int scoops = GetScoops();
            List<string> toppings = GetToppings();

            decimal total = scoops + 0.5M * toppings.Count;
            textBoxTotal.Text = total.ToString("c2");
        }

        private List<string> GetToppings()
        {
            List<string> toppings = new List<string>();

            if (checkBoxSprinkles.Checked)
            {
                toppings.Add("Sprinkles");
            }
            if (checkBoxOreos.Checked)
            {
                toppings.Add("Oreos");
            }
            if (checkBoxChocolateChips.Checked)
            {
                toppings.Add("Chocolate Chips");
            }
            if (checkBoxCherrySyrup.Checked)
            {
                toppings.Add("Cherry Syrup");
            }
            if (checkBoxGummyWorms.Checked)
            {
                toppings.Add("Gummy Worms");
            }
            return toppings;
        }
       

        private int GetScoops()
        {
            if (radioButton1Scoop.Checked)
            {
                return 1;
            }
            if (radioButton2Scoops.Checked)
            {
                return 2;
            }
            return 3;
        }

        private void radioButton1Scoop_CheckedChanged(object sender, EventArgs e)
        {
            DisplayTotal();
        }

        private void radioButton2Scoops_CheckedChanged(object sender, EventArgs e)
        {
            DisplayTotal();
        }

        private void radioButton3Scoops_CheckedChanged(object sender, EventArgs e)
        {
            DisplayTotal();
        }

        private void checkBoxSprinkles_CheckedChanged(object sender, EventArgs e)
        {
            DisplayTotal();
        }

        private void checkBoxOreos_CheckedChanged(object sender, EventArgs e)
        {
            DisplayTotal();
        }

        private void checkBoxChocolateChips_CheckedChanged(object sender, EventArgs e)
        {
            DisplayTotal();
        }

        private void checkBoxCherrySyrup_CheckedChanged(object sender, EventArgs e)
        {
            DisplayTotal();
        }

        private void checkBoxGummyWorms_CheckedChanged(object sender, EventArgs e)
        {
            DisplayTotal();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            InitializeSelections();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonPrintOrder_Click(object sender, EventArgs e)
        {
            List<string> flavors = GetFlavors();
            int scoops = GetScoops();
              List<string> toppings = GetToppings();

            decimal total = scoops + 0.5M * toppings.Count;

            OrderForm orderForm = new OrderForm(flavors, scoops, toppings, total);
            orderForm.MdiParent = _parent;
            orderForm.Show();
        }

        private List<string> GetFlavors()
        {
            List<string> flavors = new List<string>();

            foreach (var item in listBoxFlavors.SelectedItems)
            {
                flavors.Add(item.ToString());
            }
            return flavors;
        }
    }
}
