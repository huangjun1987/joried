﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IceCreamStore
{
    public partial class OrderForm : Form
    {
        public OrderForm(List<string> flavors, int scoops, List<string> toppings, decimal total)
        {
            InitializeComponent();
            labelFlavors.Text = string.Join(", ", flavors); 
            labelScoops.Text = scoops.ToString();
            labelToppings.Text = string.Join( ", ", toppings);
            labelTotal.Text = total.ToString("c2");
        }
    }
}
