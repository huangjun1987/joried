﻿namespace IceCreamStore
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.listBoxFlavors = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton3Scoops = new System.Windows.Forms.RadioButton();
            this.radioButton2Scoops = new System.Windows.Forms.RadioButton();
            this.radioButton1Scoop = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.checkBoxGummyWorms = new System.Windows.Forms.CheckBox();
            this.checkBoxCherrySyrup = new System.Windows.Forms.CheckBox();
            this.checkBoxChocolateChips = new System.Windows.Forms.CheckBox();
            this.checkBoxOreos = new System.Windows.Forms.CheckBox();
            this.checkBoxSprinkles = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxTotal = new System.Windows.Forms.TextBox();
            this.buttonPrintOrder = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Flavors:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(140, 10);
            this.label2.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Scoops:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(285, 10);
            this.label3.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Toppings:";
            // 
            // listBoxFlavors
            // 
            this.listBoxFlavors.FormattingEnabled = true;
            this.listBoxFlavors.Items.AddRange(new object[] {
            "Chocolate",
            "Vanilla",
            "Cookie dough",
            "Strawberry",
            "Mint chip",
            "Mocha",
            "Cookies n cream"});
            this.listBoxFlavors.Location = new System.Drawing.Point(24, 42);
            this.listBoxFlavors.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.listBoxFlavors.Name = "listBoxFlavors";
            this.listBoxFlavors.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBoxFlavors.Size = new System.Drawing.Size(97, 134);
            this.listBoxFlavors.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Info;
            this.groupBox1.Controls.Add(this.radioButton3Scoops);
            this.groupBox1.Controls.Add(this.radioButton2Scoops);
            this.groupBox1.Controls.Add(this.radioButton1Scoop);
            this.groupBox1.Location = new System.Drawing.Point(142, 42);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.groupBox1.Size = new System.Drawing.Size(117, 138);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // radioButton3Scoops
            // 
            this.radioButton3Scoops.AutoSize = true;
            this.radioButton3Scoops.Location = new System.Drawing.Point(8, 85);
            this.radioButton3Scoops.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.radioButton3Scoops.Name = "radioButton3Scoops";
            this.radioButton3Scoops.Size = new System.Drawing.Size(68, 17);
            this.radioButton3Scoops.TabIndex = 4;
            this.radioButton3Scoops.TabStop = true;
            this.radioButton3Scoops.Text = "3 scoops";
            this.radioButton3Scoops.UseVisualStyleBackColor = true;
            this.radioButton3Scoops.CheckedChanged += new System.EventHandler(this.radioButton3Scoops_CheckedChanged);
            // 
            // radioButton2Scoops
            // 
            this.radioButton2Scoops.AutoSize = true;
            this.radioButton2Scoops.Location = new System.Drawing.Point(8, 54);
            this.radioButton2Scoops.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.radioButton2Scoops.Name = "radioButton2Scoops";
            this.radioButton2Scoops.Size = new System.Drawing.Size(68, 17);
            this.radioButton2Scoops.TabIndex = 3;
            this.radioButton2Scoops.TabStop = true;
            this.radioButton2Scoops.Text = "2 scoops";
            this.radioButton2Scoops.UseVisualStyleBackColor = true;
            this.radioButton2Scoops.CheckedChanged += new System.EventHandler(this.radioButton2Scoops_CheckedChanged);
            // 
            // radioButton1Scoop
            // 
            this.radioButton1Scoop.AutoSize = true;
            this.radioButton1Scoop.Location = new System.Drawing.Point(8, 21);
            this.radioButton1Scoop.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.radioButton1Scoop.Name = "radioButton1Scoop";
            this.radioButton1Scoop.Size = new System.Drawing.Size(63, 17);
            this.radioButton1Scoop.TabIndex = 2;
            this.radioButton1Scoop.TabStop = true;
            this.radioButton1Scoop.Text = "1 scoop";
            this.radioButton1Scoop.UseVisualStyleBackColor = true;
            this.radioButton1Scoop.CheckedChanged += new System.EventHandler(this.radioButton1Scoop_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Info;
            this.groupBox2.Controls.Add(this.checkBoxGummyWorms);
            this.groupBox2.Controls.Add(this.checkBoxCherrySyrup);
            this.groupBox2.Controls.Add(this.checkBoxChocolateChips);
            this.groupBox2.Controls.Add(this.checkBoxOreos);
            this.groupBox2.Controls.Add(this.checkBoxSprinkles);
            this.groupBox2.Location = new System.Drawing.Point(287, 47);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.groupBox2.Size = new System.Drawing.Size(150, 133);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // checkBoxGummyWorms
            // 
            this.checkBoxGummyWorms.AutoSize = true;
            this.checkBoxGummyWorms.Location = new System.Drawing.Point(9, 112);
            this.checkBoxGummyWorms.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.checkBoxGummyWorms.Name = "checkBoxGummyWorms";
            this.checkBoxGummyWorms.Size = new System.Drawing.Size(94, 17);
            this.checkBoxGummyWorms.TabIndex = 9;
            this.checkBoxGummyWorms.Text = "Gummy worms";
            this.checkBoxGummyWorms.UseVisualStyleBackColor = true;
            this.checkBoxGummyWorms.CheckedChanged += new System.EventHandler(this.checkBoxGummyWorms_CheckedChanged);
            // 
            // checkBoxCherrySyrup
            // 
            this.checkBoxCherrySyrup.AutoSize = true;
            this.checkBoxCherrySyrup.Location = new System.Drawing.Point(9, 88);
            this.checkBoxCherrySyrup.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.checkBoxCherrySyrup.Name = "checkBoxCherrySyrup";
            this.checkBoxCherrySyrup.Size = new System.Drawing.Size(84, 17);
            this.checkBoxCherrySyrup.TabIndex = 8;
            this.checkBoxCherrySyrup.Text = "Cherry syrup";
            this.checkBoxCherrySyrup.UseVisualStyleBackColor = true;
            this.checkBoxCherrySyrup.CheckedChanged += new System.EventHandler(this.checkBoxCherrySyrup_CheckedChanged);
            // 
            // checkBoxChocolateChips
            // 
            this.checkBoxChocolateChips.AutoSize = true;
            this.checkBoxChocolateChips.Location = new System.Drawing.Point(9, 65);
            this.checkBoxChocolateChips.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.checkBoxChocolateChips.Name = "checkBoxChocolateChips";
            this.checkBoxChocolateChips.Size = new System.Drawing.Size(102, 17);
            this.checkBoxChocolateChips.TabIndex = 7;
            this.checkBoxChocolateChips.Text = "Chocolate chips";
            this.checkBoxChocolateChips.UseVisualStyleBackColor = true;
            this.checkBoxChocolateChips.CheckedChanged += new System.EventHandler(this.checkBoxChocolateChips_CheckedChanged);
            // 
            // checkBoxOreos
            // 
            this.checkBoxOreos.AutoSize = true;
            this.checkBoxOreos.Location = new System.Drawing.Point(9, 40);
            this.checkBoxOreos.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.checkBoxOreos.Name = "checkBoxOreos";
            this.checkBoxOreos.Size = new System.Drawing.Size(54, 17);
            this.checkBoxOreos.TabIndex = 6;
            this.checkBoxOreos.Text = "Oreos";
            this.checkBoxOreos.UseVisualStyleBackColor = true;
            this.checkBoxOreos.CheckedChanged += new System.EventHandler(this.checkBoxOreos_CheckedChanged);
            // 
            // checkBoxSprinkles
            // 
            this.checkBoxSprinkles.AutoSize = true;
            this.checkBoxSprinkles.Location = new System.Drawing.Point(9, 16);
            this.checkBoxSprinkles.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.checkBoxSprinkles.Name = "checkBoxSprinkles";
            this.checkBoxSprinkles.Size = new System.Drawing.Size(69, 17);
            this.checkBoxSprinkles.TabIndex = 5;
            this.checkBoxSprinkles.Text = "Sprinkles";
            this.checkBoxSprinkles.UseVisualStyleBackColor = true;
            this.checkBoxSprinkles.CheckedChanged += new System.EventHandler(this.checkBoxSprinkles_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(142, 181);
            this.label4.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "$1.00/scoop";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(285, 181);
            this.label5.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "$.50/topping";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(287, 208);
            this.label6.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Total:";
            // 
            // textBoxTotal
            // 
            this.textBoxTotal.Location = new System.Drawing.Point(328, 205);
            this.textBoxTotal.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.textBoxTotal.Name = "textBoxTotal";
            this.textBoxTotal.ReadOnly = true;
            this.textBoxTotal.Size = new System.Drawing.Size(98, 20);
            this.textBoxTotal.TabIndex = 9;
            this.textBoxTotal.TabStop = false;
            // 
            // buttonPrintOrder
            // 
            this.buttonPrintOrder.Location = new System.Drawing.Point(163, 246);
            this.buttonPrintOrder.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.buttonPrintOrder.Name = "buttonPrintOrder";
            this.buttonPrintOrder.Size = new System.Drawing.Size(78, 33);
            this.buttonPrintOrder.TabIndex = 10;
            this.buttonPrintOrder.Text = "Print Order";
            this.buttonPrintOrder.UseVisualStyleBackColor = true;
            this.buttonPrintOrder.Click += new System.EventHandler(this.buttonPrintOrder_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(311, 246);
            this.buttonClear.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(54, 33);
            this.buttonClear.TabIndex = 11;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(401, 246);
            this.buttonExit.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(51, 33);
            this.buttonExit.TabIndex = 12;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(482, 294);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonPrintOrder);
            this.Controls.Add(this.textBoxTotal);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.listBoxFlavors);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
            this.Name = "Form1";
            this.Text = "Ice Cream Store";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listBoxFlavors;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton3Scoops;
        private System.Windows.Forms.RadioButton radioButton2Scoops;
        private System.Windows.Forms.RadioButton radioButton1Scoop;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox checkBoxGummyWorms;
        private System.Windows.Forms.CheckBox checkBoxCherrySyrup;
        private System.Windows.Forms.CheckBox checkBoxChocolateChips;
        private System.Windows.Forms.CheckBox checkBoxOreos;
        private System.Windows.Forms.CheckBox checkBoxSprinkles;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxTotal;
        private System.Windows.Forms.Button buttonPrintOrder;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonExit;
    }
}

