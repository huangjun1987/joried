﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IceCreamStore
{
    public partial class MdiContainerForm : Form
    {
        public MdiContainerForm()
        {
            InitializeComponent();
            this.IsMdiContainer = true;
            Form1 form = new Form1(this);
            form.MdiParent = this;
            form.Show();
        }
    }
}
